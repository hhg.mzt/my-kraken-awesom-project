package main

import (
    "fmt"
    "math/rand"
    "time"
)


/*
Simulars un salón de clases en donde in profsos aplicara un examen, pero el salón 
es mas pequeño de lo esperado el profesor aplicara el examén con un máximo de 
10 estudiantes, cuando un estudiante termine su examén le indicara al profesor 
para que este de la entrada a otro alumno hasta tener 10 alumnos de nuevo en el salón
*/

func main() {
    estudiantes := 10

    // buffered channel, one slot for every goroutine
    // send side can complete without receive (non-blocking)
    ch := make(chan string, estudiantes)

    // max number of RUNNING goroutines at any given time
    // g := runtime.NumCPU()
    g := 2
    // buffered channel, based on the max number of the goroutines in RUNNING state
    // added to CONTROL the number of goroutines in RUNNING state
    semaforo := make(chan bool, g)

    for e := 0; e < estudiantes; e++ {
        // Se crean 10 goroutines en un esta create 10 goroutines
        // una por cada estudiante
        go func(estudiante int) {

            //Cuando cambia el estado de una goroutine de RUNNABLE a RUNNING
            // Se envia una señal/vala el semaforo 
            // si el buffer de un canal esta lleño, este se bloquerara, por ejemplo un alumno toma asiento
            semaforo <- true
            {

                // los alumnos realizando el examen
                time.Sleep(time.Duration(rand.Intn(200)) * time.Millisecond)

                // cuando ya un alumno ha terminado el examen, se envia señal al canal
                ch <- "prueba finalizada"
                fmt.Println("Estudiante: ha terminado la prueba", estudiante)
            }


            // cuando se han finalizado los examenes obtener el valor del canal semaforo
            // se genera otra gorutina para que otro alumne inicie su examen
            <-semaforo
        }(e)
    }

    // Se espera a que se apliquen todos los examenes
    for estudiantes > 0 {
        // señal enviada por estudiante se ha recibido
        p := <-ch

        estudiantes--
        fmt.Println(p)
        fmt.Println("Profesor: ha recibido la señal del estudiante : ", estudiantes)

    }
}