import requests
from bs4 import BeautifulSoup

ulr_google_noticias = 'https://news.google.com/topstories?hl=es-419&gl=MX&ceid=MX:es-419'

def get_main_news():
    url = 'https://www.cmmedia.es/noticias/castilla-la-mancha/'
    respuesta = requests.get(
        url,
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
        }
    )
    '''
    print(respuesta.status_code)
    print(respuesta.text)
    print(respuesta.headers)
    print(respuesta.request.headers)
    print(respuesta.request.method)
    '''
    contenido_web = BeautifulSoup(respuesta.text, 'lxml')
    noticias = contenido_web.find('ul', attrs={'class':'news-list'})
    articulos = noticias.findChildren('div', attrs={'class':'media-body'})

    noticias = []
    for articulo in articulos:
        print("============================")
        print(articulo.find('h3').a.get('href'))
        print(articulo.find(h3).get_text())
        print("=================================")
        noticias.append({
            'url': articulo.find('h3').a.get('href'),
            'titulo': articulo.find('h3').get_text()
        })
    return noticias

def launch_request(url):
    try:
        respuesta = requests.get(
            url,
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
            }
        )
        respuesta.raise_for_status()
    except requests.exceptions.HTTPError as err:
        raise SystemExit(err)

    return respuesta

def get_all_info_by_new(noticia):
    print(f'Scrapping {noticia["titulo"]}')
    respuesta = launch_request(noticia["url"])
    contenido_web = BeautifulSoup(respuesta.text, 'lxml')
    articulo = contenido_web.find('article', attrs={'class':'post'})
    noticia['fecha'] = articulo.find('time').get_text()
    noticia['articulo'] = articulo.find('div', attrs={'class':'', 'id':''}).get_text()


if __name__  ==  '__main__':
    noticias = get_main_news()

    for noticia in noticias:
        noticia = get_all_info_by_new(noticia)
        print('=================================')
        print(noticia)
        print('=================================')



