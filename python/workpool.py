from datetime import datetime
from multiprocessing import Pool, Queue, Process, Manager
import random
import signal
import time

num_escritores = 10
num_lectores = 3

def generar_frases() -> tuple:
    date = datetime.now()
    dias_trasncurridos = date - datetime(2022, 1, 1, 0, 0)
    dias_faltantes = datetime(2022, 12, 31, 0, 0) - date
    primera_frase = 'Hoy es'
    segunda_frase = f'el dia {date.day} del mes {date.month}'
    tercera_frase = f'del año: {date.year}'
    cuarta_frase = f'han transcurrido: '
    quinta_frase = f'{dias_trasncurridos} desde '
    sexta_frase = f'el inicio del año {date.year}'
    septima_frase = f'faltan {dias_faltantes} para'
    octava_frase = 'terminar el año'
    novenda_frase = 'con este mensaje'
    decima_frase = 'me despido, podrás ponerlo en orden :)'
    return (primera_frase, segunda_frase, tercera_frase, cuarta_frase,
    quinta_frase, sexta_frase, septima_frase,octava_frase, novenda_frase, decima_frase,)

frases = generar_frases()

def escritor(i: int,q: Queue) -> None:
    delay = random.randint(1,10)
    time.sleep(delay)
    # Agregar el resulta al queue
    t = time.time()
    print(f'Soy un escitor {i}: {t} Frase aleatoria: {frases[i]}')
    q.put(t)

def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)

def lector(i, q):
    """
   Lector de trabajos en queue
    """
    
    # Lee el mensaje ubicado en la posicion inical
    message = q.get()

    time.sleep(3)
    print(f'Soy un lector {i}: {message} De nuevo otra frase: {frases[i]}')


if __name__ == '__main__':

    m = Manager()
    
    # crea una queue 
    q = m.Queue()
    for i in range(num_escritores):
        Process(target=escritor, args=(i,q,)).start()
    
    # Se crea un pool de multiprocesos
    p = Pool(num_lectores, init_worker)
    # Se crea un grupo de lectores en paralels y se inician
    # Lectores y escritores
    #Lectores estan limitado al tamaño del pool
    lectores = []
    for i in range(10):
        lectores.append(p.apply_async(lector, (i,q,)))
    
    # Esperar por un hilo asincronro para terminar
    try:
        [lectorr.get() for lectorr in lectores]
    except:
        print("Interrumpido :'(")
        p.terminate()
        p.join()









