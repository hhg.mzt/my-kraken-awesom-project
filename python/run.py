from flask import Flask
from concurrency import ejecutar_concurrencia
from asincronico import execute_request_banxico


app = Flask( __name__)


@app.route("/")
def index():
    html = """
        <h1>HOLa</h1>
        <p></p>
        <img src="https://www.icog.es/cursos/wp-content/uploads/2020/09/phyton.png" alt="Girl in a jacket" width="500" height="600">
    """
    return html


@app.route('/concurrencia')
def concurencia():
    html_render = ejecutar_concurrencia()
    ##html = request_url()
    html = """
        <h1>HOLa</h1>
        <p></p>
        <img src="https://www.icog.es/cursos/wp-content/uploads/2020/09/phyton.png" alt="Girl in a jacket" width="500" height="600">
    """
    return html_render


@app.route('/paralelismo')
def paralelismo():
    pass

@app.route('/asincronismo/<string:date>')
def asincronismo(date):
    data = execute_request_banxico(date)
    
    html = """
        <strong>Tipos de cambios en Pesos mexicanos</strong>
    """
    for key, value in data.items():
        html += f'<div>Moneda: {key.upper()} Tipo de Cambio: {value}</div>'
    return html

@app.route('/workpool')
def workpool():
    pass


@app.route('/semaforo')
def semaforo():
    pass


if __name__ == '__main__':
    app.run(debug=True)
