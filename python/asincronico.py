import time
import requests
import asyncio

TOKEN_BANXICO = 'd0b60d04d2c18172d09cc0bb408290534b230fc8aa01a62da20fe887d463e9ff'
'''
EJEMPLO:
Bmx-Token: e3980208bf01ec653aba9aee3c2d6f70f6ae8b066d2545e379b9e0ef92e9de25
'''

async def requests_banxico(date: str):
    headers={'Bmx-Tokn': TOKEN_BANXICO}
    url_dolares = f'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43717/datos/{date}/{date}?token={TOKEN_BANXICO}'

    url_euro = f'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF46410/datos/{date}/{date}?token={TOKEN_BANXICO}'
    url_yen = f'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF46406/datos/{date}/{date}?token={TOKEN_BANXICO}'
    url_real = f'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF57767/datos/{date}/{date}?token={TOKEN_BANXICO}' # BRASIL
    url_yuan = f'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF290383/datos/{date}/{date}?token={TOKEN_BANXICO}'
    loop = asyncio.get_event_loop()

    response_dolar = await loop.run_in_executor(None, requests.get,  url_dolares)
    response_euro = await loop.run_in_executor(None, requests.get, url_euro)
    response_yen = await loop.run_in_executor(None, requests.get, url_yen)
    response_yuan = await loop.run_in_executor(None, requests.get, url_yuan)
    
    return {
        'dolar': response_dolar.json()['bmx']['series'][0]['datos'][0]['dato'],
        'euro': response_euro.json()['bmx']['series'][0]['datos'][0]['dato'],
        'yen': response_yen.json()['bmx']['series'][0]['datos'][0]['dato'],
        'yuan': response_yuan.json()['bmx']['series'][0]['datos'][0]['dato']
    }


def execute_request_banxico(date):
    headers={'Bmx-Token': TOKEN_BANXICO}
    url_dolares = f'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43717/datos/{date}/{date}'
    loop = asyncio.new_event_loop()
    return loop.run_until_complete(requests_banxico(date))


