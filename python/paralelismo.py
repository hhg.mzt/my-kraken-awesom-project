import time
import pathlib
import multiprocessing
import concurrent.futures
import tqdm
import numpy as np
import pandas as pd
num_processes = multiprocessing.cpu_count()


fraccion_segundos = 0.0000005

current_directory = pathlib.Path(__file__).parent.absolute()
df = pd.read_csv(f'{current_directory}\data.csv')

ejecutando_normal = True




data_frame = pd.DataFrame({i: np.random.randint(1,100,size=1000) for i in ['a', 'b', 'c']})


def incrementar_monto(monto: float) -> float():
    time.sleep(fraccion_segundos)
    return monto * 1.5

def iniciar_normal():
    time_start = time.time()
    # tqdm.tqdm Barra de progreso
    df['result'] = list(tqdm.tqdm(map(incrementar_monto,df['monto'])))
    time_end = time.time()
    print(f'\nEsta operación tomo un total de: {round(time_end - time_start, 2)} segundos')

def iniciar_paralemismo():
    time_start = time.time()

    with concurrent.futures.ProcessPoolExecutor(num_processes) as ppe:
        # tqdm.tqdm Barra de progreso
        df['result'] = list(tqdm.tqdm(ppe.map(incrementar_monto, df['monto'], chunksize=10), total=df.shape[0]))

    time_end = time.time()
    print(f'\nEsta operación tomo un total de: {round(time_end - time_start, 2)} segundos')



if __name__ == '__main__':
    #ejecuion_nomarl()
    #ejecuion_en_paralelo()
    iniciar_normal()
    iniciar_paralemismo()
    #ejecucion_paralelismo()