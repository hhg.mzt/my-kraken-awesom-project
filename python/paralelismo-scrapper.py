import time
import requests
import concurrent.futures
#import numpy
#from numba import njit

URLS = (
    'https://jsonplaceholder.typicode.com/posts',
    'https://jsonplaceholder.typicode.com/comments',
    'https://jsonplaceholder.typicode.com/albums',
    'https://jsonplaceholder.typicode.com/photos',
    'https://jsonplaceholder.typicode.com/todos',
    'https://jsonplaceholder.typicode.com/users',

)

#https://j2logo.com/python/funciones-lambda-en-python/

def fetch_single(url: str) -> None:
    print(f'Fetching: {url}...')
    print(time.strftime("%H:%M:%S"))
    requests.get(url)
    time.sleep(1)
    print(f'Fetched {url}!')

def ejecutar_no_paralelismo():
    time_start = time.time()

    
    for url in URLS:
        fetch_single(url)
    

    time_end = time.time()
    print(f'\nAll done! Took {round(time_end - time_start, 2)} seconds')

def ejecutar_paralelismo():
    time_start = time.time()

    with concurrent.futures.ProcessPoolExecutor() as ppe:
        for url in URLS:
            ppe.submit(fetch_single, url)

    time_end = time.time()
    print(f'\nAll done! Took {round(time_end - time_start, 2)} seconds')

if __name__ == '__main__':
    ejecutar_no_paralelismo()
    print("="*70)
    ejecutar_paralelismo()

