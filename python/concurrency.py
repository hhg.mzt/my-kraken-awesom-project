import concurrent.futures
import time
import queue
from threading import Thread
import requests
import bs4
from bs4 import BeautifulSoup

noticias_mexico = 'https://news.google.com/rss?hl=es-419&gl=MX&ceid=MX:es-419'
noticas_estados_unidos = 'https://news.google.com/rss?hl=es-419&gl=US&ceid=US:es-419'
noticias_chile = 'https://news.google.com/rss?hl=es-419&gl=CL&ceid=CL:es-419'
noticias_argentia = 'https://news.google.com/rss?hl=es-419&gl=AR&ceid=AR:es-419'
noticias_colombia = 'https://news.google.com/rss?hl=es-419&gl=CO&ceid=CO:es-419'
noticias_cuba = 'https://news.google.com/rss?hl=es-419&gl=CU&ceid=CU:es-419'
noticias_peru = 'https://news.google.com/rss?hl=es-419&gl=PE&ceid=PE:es-419'
noticias_uruguay = 'https://news.google.com/rss?hl=es-419&gl=US&ceid=US:es-419'
noticias_venezuela = 'https://news.google.com/rss?hl=es-419&gl=VE&ceid=VE:es-419'
urls_paises = (noticias_mexico, noticas_estados_unidos, noticias_chile, noticias_argentia, noticias_colombia,noticias_cuba, noticias_peru, noticias_uruguay, noticias_venezuela,)

#thread_local = threading.local()

'''
EJECUCION DE FORMA CONCURRENTE 
HARE VARIAS TAREAS A LA VEZ SIN ESPERAR A QUE TERMINE UNA 
PARA CONTINUAR CON LA QUE SIGUE
'''

def request_url_concurrente(sites):
    items = []
    for pais in sites:
        respuesta = requests.get(noticias_mexico)
        contenido_web = BeautifulSoup(respuesta.text, 'xml')
        items.append(contenido_web.findAll('item'))
    html_render = '<div>'
    for items_pais in items:
        for item_noticia in items_pais:
            html_render += f'<div style="border-inline: 2px dotted;"><p> {item_noticia.title.text} </p> <a href= {item_noticia.link.text}>Visite<a> Fecha: {item_noticia.pubDate.text}</div>' 

    html_render += '</div>'
    return html_render

que = queue.Queue()

def ejecutar_concurrencia():
    t = Thread(target=lambda q, arg1: q.put(request_url_concurrente(arg1)), args=(que,urls_paises*1))
    t.start()
    t.join()
    return que.get()
    

''''
EJECUCION NORMAL
'''
def request_url():
    items = []
    for pais in urls_paises:
        #print(pais)
        respuesta = requests.get(noticias_mexico)
        contenido_web = BeautifulSoup(respuesta.text, 'xml')
        items.append(contenido_web.findAll('item'))
    html_render = '<div>'
    for items_pais in items:
        for item_noticia in items_pais:
            html_render += f'<div style="border-inline: 2px dotted;"><p> {item_noticia.title.text} </p> <a href= {item_noticia.link.text}>Visite<a> Fecha: {item_noticia.pubDate.text}</div>' 

    html_render += '</div>'
    return html_render
    



def get_main_news():
    respuesta = launch_request(url)
    contenido_web = BeautifulSoup(respuesta.text, 'lxml')


def launch_request(url):
    try:
        respuesta = requests.get(
            url,
            headers = {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36'
            }
        )
        respuesta.raise_for_status()
    except requests.exceptions.HTTPError as err:
        raise SystemExit(err)

    return respuesta

def get_all_info_by_new(noticia):
    pass