# Soluciones a prueba tecnica #

Este repositorio cuenta con 5 ejercicios

- Paralelismo
- Concurrencia
- Asincronismo
- Workpool
- Semaphore

Los ejercicios de paralelismo, concurrencia y asincronismo fueron construidos
con Python 3.8 todos comparten el mismo entorno virtual. El ejercicio de semaphore fue creado con Go 1.15, sin necesidad de instalar depencias.

## Ejecutar los ejemplos de Python ##
Dentro de la carpeta python crea tu entorno virtual

    virtualenv -p "ruta ejecutable python 3.8" .venv

Una vez creado **activalo** y **posteriormente** instala las **dependencias** 

    En windows (powershell) venv/scripts/activate.ps1
	En linux source venv/bin/activate
	Para instalalar las dependencias
	pip install -r requirements.txt

Los ejemplos de concurrencias y asincronismo se ejecutan desde el navegado (hacen uso del framework web Flask). 

	Lanzar el servidor de flask
	python run.py

En la direccion ip de localhost y el puerto 5000 dirigete al navegador web de tu preferencia

Para ver el ejemplo de **concurrencia**: 
http://localhost:5000/concurrencia se renderiza un html que muestra los resultados de un scrapper a los sitios de noticias de google news de varios paises mediante un objeto Thread que recibe la función que realiza el scrapper
Ejemplo de **asincronismo**:
http://localhost:5000/asincronismo/2022-03-30 se un fecha dada en formato 
yyyy-MM-dd, trae los tipos de cambios de un API de Banxico, nuevamente es otro ejemplo de scrapper con la sintaxis async await proporcionada por asyncio. 

Para ejecutar el ejemplo de woorkpool en la carpeta python ejecutar **python workpool.py**. El script crea un Pool de workes y depliega mensajes para la nocion de que no importa el orden sino la ejecución se muestra frases e indicies no necesariamente en orde.

Para ejecutar el ejemplo de paralelismo dentro de la carpeta de python ejecutar python **python paralelismo.py**. Este ejemplo toma al archivo en raiz datos.csv  en donde se iterarara para aplicar un aumento a la columna monto el script ejecuta la iteracion de dos formas normal y con paralelismo para ver por pantalla la comparacion entre el tiempo que le toma hacer a cada función la misma tarea. Ademas cuenta con un progrees bar e ambas ejecuciones y un contador de tiempo para compara.


## Ejemplo en Golang (semaphore) ##
Este ejercio fue construido con Go version 1.15 usando las caracteriscas de channels del lenguaje. Se trata de una simulación de un salón de clases pequeño en donde el profesor va pasando de 10 en 10 a los alumnos para realizar un examen.
Usa la biblioteca estanda de go porque no es necesario instalar ninguna depencencia.
En la carpete de **golang** ejecutar **go run main.go** mostrara por termina el resultado de lanzar el semaforo o bien se lo prefieres puedes construir un ejecutable  **go build main.go** pudiendose ejecutar el .exe desde la terminal  **./main.exe** o bien abrirlo como cualquier aplicación.